/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <Windows.h>
#include <stdint.h>
#include <stdio.h>

typedef void* (__fastcall *ResourceAsStream_t)(uint32_t ecx, uint32_t edx, char* name, uint32_t unk1, uint32_t unk2);
typedef void* (*Allocate_t)(size_t size, uint32_t type, void* category, uint32_t unk1, uint32_t unk2, uint32_t unk3);
typedef void* (*GetAllocator_t)();
typedef void* (__fastcall *CreateStream_t)(void* stream, void* edx, void* data, size_t length, uint32_t unk1, uint32_t unk2, void* allocator, char* name);

ResourceAsStream_t ResourceAsStream = NULL;
Allocate_t Allocate = NULL;
GetAllocator_t GetAllocator = NULL;
CreateStream_t CreateStream = NULL;

void** AllocatorCategory_Global = NULL;
char* ResourceName = NULL;
char* StreamName = NULL;

char certOverride[262144] = {0};

void* __fastcall ResourceAsStream_detour(uint32_t ecx, uint32_t edx, char* name, uint32_t unk1, uint32_t unk2) {
    if (strcmp(name, ResourceName) == 0) {
        void* stream = Allocate(0x30, 0, *AllocatorCategory_Global, 1, 0, 0);
        return CreateStream(stream, NULL, (void*)certOverride, strlen(certOverride), 0, 1, GetAllocator(), StreamName);
    }
    return ResourceAsStream(ecx, edx, name, unk1, unk2);
}

void DoPatch(void* addr) {
    DWORD oldProtect;
    VirtualProtect((void*)addr, 4, PAGE_EXECUTE_READWRITE, &oldProtect);
    *((size_t*)addr) = (size_t)ResourceAsStream_detour;
    VirtualProtect((void*)addr, 4, oldProtect, &oldProtect);
}

BOOLEAN WINAPI DllMain(HINSTANCE hDllHandle, DWORD nReason, LPVOID Reserved) {
    if (nReason == DLL_PROCESS_ATTACH) {
        FILE *certFile = fopen("cacert.pem", "r");
        if (certFile == NULL) {
            // Failed to open cert file
            return TRUE;
        }

        if (fread(certOverride, 1, sizeof(certOverride)-1, certFile) == 0) {
            // Read failed or empty file
            fclose(certFile);
            return TRUE;
        }

        fclose(certFile);

        size_t base = (size_t)GetModuleHandle(NULL);

        ResourceAsStream = (ResourceAsStream_t)(base+0x128060);
        Allocate = (Allocate_t)(base+0x21c2a0);
        GetAllocator = (GetAllocator_t)(base+0x21d0c0);
        CreateStream = (CreateStream_t)(base+0x18f150);

        AllocatorCategory_Global = (void**)(base+0x881790);
        ResourceName = (char*)(base+0x786be4);
        StreamName = (char*)(base+0x783c68);

        DoPatch((void*)(base+0x786964));
    }
    return TRUE;
}