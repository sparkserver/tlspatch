# TLSPatch
An ASI hook for NFS: World that lets you replace the HTTPS CA certs used for server validation.

# Building
The [Meson](https://mesonbuild.com) build system is used.
```
meson builddir .
ninja -C builddir
```

# Usage
You need to have an ASI loader installed, for example the [Ultimate ASI Loader](https://github.com/ThirteenAG/Ultimate-ASI-Loader).
Place `TLSPatch.asi` in `scripts/` folder. In the same folder put your CA cert bundle named as `cacert.pem`.
You can use [CURL's CA bundle](https://curl.haxx.se/docs/caextract.html).

# Notes on the game's TLS support
* Latest supported TLS version is 1.0
* Supported cipher suites:
```
TLS_DHE_RSA_WITH_AES_256_CBC_SHA
TLS_DHE_DSS_WITH_AES_256_CBC_SHA
TLS_RSA_WITH_AES_256_CBC_SHA
TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA
TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA
TLS_RSA_WITH_3DES_EDE_CBC_SHA
TLS_DHE_RSA_WITH_AES_128_CBC_SHA
TLS_DHE_DSS_WITH_AES_128_CBC_SHA
TLS_RSA_WITH_AES_128_CBC_SHA
TLS_RSA_WITH_IDEA_CBC_SHA
TLS_RSA_WITH_RC4_128_SHA
```
* SNI is not supported so you can't have multiple vhosts on the same port